# Copyright 2014-2017 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ release=${PV} suffix=tar.bz2 ]

SUMMARY="General purpose malloc(3) implementation"
DESCRIPTION="
jemalloc is a general purpose malloc(3) implementation that emphasizes fragmentation avoidance and
scalable concurrency support.
"
HOMEPAGE+=" http://${PN}.net"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="valgrind"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt [[ note = [ For xsltproc ] ]]
    build+run:
        valgrind? ( dev-util/valgrind )
"

UPSTREAM_CHANGELOG="https://raw.githubusercontent.com/${PN}/${PN}/master/ChangeLog"
UPSTREAM_DOCUMENTATION="
    http://www.canonware.com/download/${PN}/${PN}-latest/doc/${PN}.html [[ description = [ Manual page ] ]]
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'valgrind' )

